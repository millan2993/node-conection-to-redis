var redis = require('redis');
var client = redis.createClient(6379, '172.17.0.4');

client.on('error', function(err){
  console.log('Something went wrong ', err)
});

client.set('name', 'Juan');

client.get('name', function(error, result) {
  if (error) throw error;
  console.log('GET result ->', result)
});